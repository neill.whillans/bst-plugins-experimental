from setuptools import setup

# Ideally we could do this with pyproject.toml
# but the setuptools version is just too old
setup(
    name="mypackage",
    version="0.0.1",
    packages=["mypackage"],
)
